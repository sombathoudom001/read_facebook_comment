import {useRef,useState} from 'react';
import moment from 'moment';

const App = () => {
  const inputVideoId = useRef(null);
  const inputToken = useRef(null);
  const [message, setMessage] = useState([]);
  const [status, setStatus] = useState('No Action');
  const [btnDisable, setBtnDisable] = useState(false);
  const handleSubmit = (e) => {
    if(!inputVideoId.current.value) return alert('Please pass video ID');
    if(!inputToken.current.value) return alert('Please pass User Token');
    setStatus('Processing...');
    const source = new EventSource(`https://streaming-graph.facebook.com/${inputVideoId.current.value}/live_comments?access_token=${inputToken.current.value}&comment_rate=one_per_two_seconds&fields=from{name,id},message,created_time`,{withCredentials: true,'Access-Control-Allow-Origin':true});
    source.onmessage = function(event) {
      setMessage(current => [...current,JSON.parse(event?.data)]);
      setBtnDisable(true);
    };
    source.onerror = (event) => { 
      source.close();
      return alert('Error! Please check Video ID or UserToken...!');
    };
  }
  return (
    <div className="App">
       <div className="container">
          <div className="row">
            <div className="col-md-12">
              <center><h1>Facebook Live Comment</h1></center> 
            </div>
          </div>
          <div className="row">
            <div className="col-md-4 mb-3">
              <label htmlFor="videoID" className="form-label">Video ID</label>
              <input type="text" className="form-control" id="videoID" placeholder="Paste video ID here" ref={inputVideoId}/>
            </div>
            <div className="col-md-8">
              <label htmlFor="userAccessToken" className="form-label">Access Token</label>
              <input type="text" className="form-control" id="userAccessToken" placeholder="UserAccessToken" ref={inputToken}/>
            </div>
          </div>
          <div className="row mb-3">
            <h6><span className="badge text-bg-primary">Developed by:</span><a href="https://www.facebook.com/WeAreKPlusTechnology" target="_blank" role="button"><span className="badge text-bg-warning  mx-2">K-plus Technology</span></a></h6>
            <div className="col-md-12 d-grid">
              <button className="btn btn-success" disabled={btnDisable} onClick={handleSubmit}>Submit</button>
            </div>
          </div>
          <div className="row">
             <div className="col-md-12">
                <div className="card">
                  <div className="card-body">
                    <div className='table-responsive-md'>
                      {message.length ? 
                      <table className="table">
                        <thead>
                          <tr>
                            <th scope="col">#</th>
                            <th scope="col">From Account</th>
                            <th scope="col">Message</th>
                            <th scope="col">Created_At</th>
                          </tr>
                        </thead>
                        <tbody>
                          {message.map((ele,index) => {
                            return (<tr key={index}>
                              <th scope="row">{index+1}</th>
                              <td>{ele.from.name}</td>
                              <td>{ele.message}</td>
                              <td width="189px">{moment(ele.created_time).format('YYYY-MM-DD h:mm:ss a')}</td>
                          </tr>)
                          })}
                        </tbody>
                      </table> 
                      :<center>{status}</center> }
                    </div>
                  </div>
                </div>
              </div>
          </div>
        </div>
    </div>
  );
}

export default App;
